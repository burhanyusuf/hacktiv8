# Game Arena

## Kompetensi
- Basic Class
- Abstraction
- Module Exports
- Encapsulation
- Inheritance
- Polymorphism
- Aggregation
- Composition
- Factory (like)

## PLAYER

| type    | attack | life |
| ------- | ------ | ---- |
| soldier | 100    | 600  |
| archer  | 80     | 800  |
| giant   | 200    | 1000 |
| enemy   | 20     | 100  |

Perilaku: 

- Player punya `healing()` (berbeda tingkat healing tiap jenis player ke defense)
- Player punya `greeting ()` (berbeda tiap player)
- Player punya status `isDead`

## ARENA

| type    | max_player | max_enemy |
| ------- | ---------- | --------- |
| forest  | 8          | 16        |
| desert  | 6          | 12        |
| castile | 5          | 10        |

Aksi: 

- Arena punya `addPlayer()` 
  - Memasukkan 1 Player ke dalam arena
  - Tidak bisa dijalankan lagi jika sudah penuh atau melebihi kapasitas
- Arena punya `createEnemy(number)` 
  - Method ini hanya bisa dijalankan sekali (coba gunakan flagging)
  - Memasukkan Enemy (player jenis enemy) ke dalam arena berdasarkan number
  - Tidak bisa create kalau number melebihi kapasitas
  - Nama enemy: `enemy1`, `enemy2`, `enemy3`
- Arena punya `playersAttack()`
  - Masing masing Player menyerang Enemy
  - Tidak bisa dijalankan jika Player atau Enemy kosong
  - Melakukan output untuk setiap Enemy (minimal berisi status dan jumlah life/nyawa)
  - Nyawa/life Enemy tidak boleh minus (minimal 0)
- Arena punya `enemyAttack()` 
  - Masing masing Enemy menyerang Player
  - Tidak bisa dijalankan jika Player atau Enemy kosong
  - Melakukan output untuk setiap Player (minimal berisi status dan jumlah life/nyawa)
  - Nyawa/life Player tidak boleh minus (minimal 0)
- Arena punya `healing()` 
  - Masing masing player yang belum mati bisa melakukan healing()
  - Method ini hanya bisa dijalankan sekali (coba gunakan flagging)
- Arena punya `report()` 
  - Menampilkan status setiap Player dan Enemy (greeting, jumlah player alive dan enemy alive)
- Arena punya `destroy()`
  - Mengosongi isi arena dari players dan enemies

Arena dijalankan secara Chaining
