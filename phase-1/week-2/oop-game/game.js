const { Soldier, Archer, Giant, Enemy } = require("./player");
const { Forest, Desert, Castile } = require("./arena");

const player1 = new Soldier("Budi");
const player2 = new Archer("Citra");
const player3 = new Giant("Doni");
const player4 = new Enemy("Edo");


const forest = new Forest();
forest.addPlayer(player1).addPlayer(player2).addPlayer(player3).createEnemies(5).playersAttack();

console.log(forest);