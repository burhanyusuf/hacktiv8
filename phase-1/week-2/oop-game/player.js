class Player {
    constructor(){

    }
}

class Soldier extends Player {
    constructor(){
        super();
    }
}

class Archer extends Player {
    constructor(){
        super();
    }
}

class Giant extends Player {
    constructor(){
        super();
    }    
}

class Enemy extends Player {
    constructor(){
        super();
    }  
}

module.exports {
    Enemy,
    Giant,
    Archer,
    Soldier,
    Player
}