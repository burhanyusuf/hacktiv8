class FruitTree {
  constructor(growingAge, matureAge, deadAge, additionalHeight) {
    this._age = 0;
    this._deadAge = deadAge;
    this._matureAge = matureAge;
    this._growingAge = growingAge;
    this._height = 0; // in cm
    this._fruits = [];
    this._harvested = [];
    this._healthStatus = true;
    this._additionalHeight = additionalHeight;
  }

  get age() {
    return this._age;
  }

  get height() {
    return this._height / 100; // convert to m
  }

  get fruits() {
    return this._fruits;
  }

  get healthStatus() {
    if (this._age >= this._deadAge) {
      this._healthStatus = false;
    }
    return this._healthStatus;
  }

  get harvested() {
    return `${this._harvested.length} (${
      this._harvested.filter((fruit) => fruit.quality == "good").length
    } good, ${
      this._harvested.filter((fruit) => fruit.quality != "good").length
    } bad)`;
  }

  get harvestedFruits() {
    return this._harvested;
  }

  // Get current states here

  // Grow the tree
  grow() {
    this._age++;
    if (this._age <= this._growingAge)
      this._height += this._additionalHeight || Math.round(Math.random() * 50);
  }

  // Produce some mangoes
  produce() {
    if (this._age < this._matureAge) return;
    for (let i = 0; i < Math.round(Math.random() * 10); i++) {
      this._fruits.push(new Fruit());
    }
  }

  // Get some fruits
  harvest() {
    this._harvested = [...this._fruits];
    this._fruits = [];
  }

  checkIfMature() {
    if (this._age <= this._matureAge) return false;
    return true;
  }
}
class Fruit {
  constructor() {
    let qualityVerbs = ["good", "bad"];
    this._quality = qualityVerbs[Math.round(Math.random())];
  }
  get quality() {
    return this._quality;
  }
}

class MangoTree extends FruitTree {
  // Initialize a new MangoTree

  constructor(growingAge, matureAge, deadAge, additionalHeight) {
    super(growingAge, matureAge, deadAge, additionalHeight);
  }

  produce() {
    if (!super.checkIfMature()) return;
    for (let i = 0; i < Math.round(Math.random() * 10); i++) {
      this._fruits.push(new Mango());
    }
  }
}

class Mango extends Fruit {
  // Produce a mango
  constructor() {
    super();
  }
}

let mangoTree = new MangoTree(2, 5, 8, 100);
console.log(`The ${mangoTree.constructor.name} is Alive`);
do {
  mangoTree.grow();
  mangoTree.produce();
  mangoTree.harvest();
  console.log(
    `[Year ${mangoTree.age} Report] Height = ${mangoTree.height} m | Fruits harvested = ${mangoTree.harvested}`
  );
} while (mangoTree.healthStatus != false);
console.log("The Tree has mets its end");
