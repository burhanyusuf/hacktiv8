class cat{

    tiredness = 0;
    hunger = 0;
    lonliness = 0;
    happiness = 0;

    constructor(){

    }

    feed(status){
        this.hunger += status;
    }

    sleep(status){
        this.tiredness += status;
    }

    pet(status){
        this.lonliness += status;
        this.happiness += status;
    }

    getHungerStatus(){
        if(this.hunger > 0){
            return 'Ngelih';
        }else{
            return 'Ora Ngelih';
        }
    }
}

let kucing = new cat();

let kucing1 = new cat();

kucing.feed(100); //nambah ngelih;
kucing.sleep(100);
kucing.pet(100);


console.log(kucing.getHungerStatus());

kucing.feed(-101);

console.log(kucing.getHungerStatus());

// class => cat;
// object => kucing;