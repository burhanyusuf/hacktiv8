function order(words){
    let output = {};
    words = words.split(' ');
    for(let i=0;i<words.length;i++){
        for(let j=0;j<words[i].length;j++){
            if(!isNaN(words[i][j])){
                output[words[i][j]] = words[i];
                break;
            }
        }
    }
    let result = Object.values(output).join(' ');
    return result;
    
}
console.log(order("is2 Thi1s T4est 3a"), "Thi1s is2 3a T4est")
// console.log(order("4of Fo1r pe6ople g3ood th5e the2"), "Fo1r the2 g3ood 4of th5e pe6ople")
// console.log(order(""), "")