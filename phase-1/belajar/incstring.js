function incrementString (string) {
    // return incrementedString
    let result = ""
    let isNumberFound = false;
    let number = "";
    for(let i=0;i<string.length;i++){
        if(isNumberFound){
            number+= string[i];
            continue;
        }
        if(isNaN(string[i])){
            result += string[i];
        }else{
            number += string[i];
            isNumberFound = true;
        }
    }
    let numberLength = number.length;
    number = Number(number) + 1;
    result += isNumberFound ? String(number).padStart(numberLength, '0') : 1;

    return result;

}

console.log(incrementString("foobar000"));