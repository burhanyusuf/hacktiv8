
var fs = require('fs');

var faker = require('faker');

class Person{

    constructor(nama,alamat,organisasi,tanggal){

        this.nama = nama;
        this.alamat = alamat;
        this.organisasi = organisasi;
        this.tanggal = tanggal;

    }

}

class PersonParser{

    constructor(fileName){

        this.fileName = fileName;
        this.persons = [];
        this.csvString = fs.readFileSync(fileName,'utf8');
        this.populateFromCSV();
    }

    addPerson(person){

        this.persons.push(person);

    }

    populateFromCSV(){

        let splitPerRow = this.csvString.split('\n');

        let splitPerColumn = splitPerRow.map(row => row.split(';'));

        for(let data of splitPerColumn){
            let person = new Person(data[0],data[1],data[2],data[3]);
            this.persons.push(person);
        }

        return ;

    }

    getPerson(){
        return this.persons;
    }

    save(){

        for(let i=0;i<this.persons.length;i++){

            let person = this.persons[i]
            let personString = `${person.nama};${person.alamat};${person.organisasi};${person.tanggal}\n`;

            fs.appendFileSync(this.fileName, personString);   
        } 

    }
}

let personParser = new PersonParser('person.csv');

console.log(personParser.getPerson());


let date = new Date('2020-01-01');

for(let i=0;i<10;i++){
    personParser.addPerson(new Person(faker.name.findName(),faker.address.city(),faker.name.jobType(),date.toString()));
    date.setDate(date.getDate() + 1);
}

personParser.save();