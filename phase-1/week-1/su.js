class game {
  constructor(string, row, column) {
    this.row = row;
    this.column = column;
    this.initialBoard = [];
    this.board = [];
    this.possibleNumbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
    this.possibilityEachCells = [];
    this.possibilityCombination = [];
    this.emptyCells = [];
    this.generateBoard(string);
    this.generatePossibility();
  }

  generateBoard(string) {
    let count = 0;
    for (let i = 0; i < this.row; i++) {
      let currentRow = [];
      for (let j = 0; j < this.column; j++) {
        currentRow.push(string[count]);
        if (string[count] == 0) {
          let emptyCell = {};
          emptyCell.column = j;
          emptyCell.row = i;
          emptyCell.possiblity = [];
          this.emptyCells.push(emptyCell);
        }
        count++;
      }
      this.board.push(currentRow);
      this.initialBoard.push(currentRow);
    }
  }

  generatePossibility() {
    let index = 0;
    for (let emptyCell of this.emptyCells) {
      this.fillPossibility(index, emptyCell.row, emptyCell.column);
      index++;
    }
  }

  attempt() {
    for (let i=0;i<this.emptyCells.length;i++ ) {
      let emptyCell = this.emptyCells[i];
      for (let number of emptyCell.possiblity) {
          if(this.isOk(emptyCell.row,emptyCell.column,number)){
              this.emptyCells.splice(i);
              this.board[emptyCell.row][emptyCell.column] = number;
              if(this.attempt()){
                  return true;
              }else{
                this.board[emptyCell.row][emptyCell.column] = 0;
                this.emptyCells.splice(i,0,emptyCell);
              }
          }
      }
    }
    return true;
  }

  isInRow(row, number) {
    for (let i = 0; i < 9; i++) {
      if (this.board[row][i] === number) {
        return true;
      }
    }
    return false;
  }

  // We check if a possible number is already in a column
  isInCol(col, number) {
    for (let i = 0; i < 9; i++) {
      if (this.board[i][col] === number) {
        return true;
      }
    }
    return false;
  }

  // We check if a possible number is in its 3x3 box
  isInBox(row, col, number) {
    // Get the boundaries of the block
    const r = row - (row % 3);
    const c = col - (col % 3);

    for (let i = r; i < r + 3; i++) {
      for (let j = c; j < c + 3; j++) {
        if (this.board[i][j] === number) {
          return true;
        }
      }
    }
    return false;
  }

  // Combined method to check if a number possible to a row,col position is ok
  isOk(row, col, number) {
    return (
      !this.isInRow(row, number) &&
      !this.isInCol(col, number) &&
      !this.isInBox(row, col, number)
    );
  }

  fillPossibility(index, row, column) {
    let rowFilled = this.board[row];
    let columnFilled = this.board.map((value, index) => value[column]);
    let squareFilled = [];
    let rowBorder = row - (row % 3);
    let colBorder = column - (column % 3);

    for (let i = rowBorder; i < rowBorder + 3; i++) {
      for (let j = colBorder; j < colBorder + 3; j++) {
        squareFilled.push(this.board[i][j]);
      }
    }
    let possibleNumber = this.possibleNumbers.filter(
      (number) =>
        !rowFilled.includes(number) &&
        !columnFilled.includes(number) &&
        !squareFilled.includes(number)
    );
    this.emptyCells[index].possiblity = [...possibleNumber];
  }

  printBoard(pattern) {
    for (let i = 0; i < this.row; i++) {
      if (i % 3 == 0) {
        console.log("|---|---|---|");
      }
      let columnString = "";
      for (let j = 0; j < this.column; j++) {
        if (j % 3 == 0) {
          columnString += "|";
        }
        columnString += pattern[i][j];
      }
      console.log(columnString + "|");
    }
    console.log("|---|---|---|");
  }
}

let sudoku = new game(
  "094020000608000000000089400060000000002000000070401800000000750900052006100908002",
  9,
  9
);
// sudoku.printBoard(sudoku.board);
// console.log(sudoku.attempt());
// console.log('Solved')

sudoku.attempt();
sudoku.printBoard(sudoku.board);
