const express = require('express')
const app = express()
const port = 3001

var cors = require('cors')
app.use(cors())
const axios = require('axios');
let getCurrentTimestamp = () => {
    return "" + Math.round(new Date().getTime() / 1000);
};
app.get('/', (req, res) => {
    axios({
        // Below is the API URL endpoint
        url: "https://app.sandbox.midtrans.com/snap/v1/transactions",
        method: "post",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization:
            "Basic " +
            Buffer.from("SB-Mid-server-hrPFTY_gPDnF0eszAt22Pbpq").toString("base64")
          // Above is API server key for the Midtrans account, encoded to base64
        },
        data:
          // Below is the HTTP request body in JSON
          {
            transaction_details: {
              order_id: "order-csb-" + getCurrentTimestamp(),
              gross_amount: 10000
            },
            credit_card: {
              secure: true
            },
            customer_details: {
              first_name: "Johny",
              last_name: "Kane",
              email: "testmidtrans@mailnesia.com",
              phone: "08111222333"
            }
          }
      }).then( snapResponse => { 
          let snapToken = snapResponse.data.token;
          res.send(snapToken);
        })
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})