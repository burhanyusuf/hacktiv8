const express = require('express')
const app = express()
const port = 3000
const MongoClient = require('mongodb').MongoClient

const connectionString = 'mongodb://localhost:27017/'

MongoClient.connect(connectionString, { useUnifiedTopology: true })
  .then(client => {

    const db = client.db('testingmongo')
    app.get('/', (req, res) => {
        db.collection('quotes').find().toArray()
        .then(results => {
          res.send(results)
        })
        .catch(error => console.error(error))
    })
    app.listen(port, () => {
        console.log(`Example app listening at http://localhost:${port}`)
    })
  }).catch(err => {

  })