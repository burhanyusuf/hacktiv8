class mobil{
    merk;
    harga;
    statusFuel = 100;

    constructor(merk,harga){
        this.merk = merk;
        this.harga = harga;
    }

    checkFuel(){
        return this.statusFuel;
    }

    updateFuel(jarak){
        this.statusFuel -= ( jarak / 10 );
        return this.checkFuel();
    }

    fillFuel(liter){
        this.statusFuel += liter;
        return this.checkFuel();
    }
}

let mobilPertamaSaya = new mobil('BMW',1000000);
console.log(mobilPertamaSaya.updateFuel(10)) // mobil melakukan perjalanan sejauh 10 km , dan akan mengembalikan nilai bahan bakar sekarang yaitu = 100 - ( 10 / 10 ) = 99
console.log(mobilPertamaSaya.fillFuel(10) )