/**
 * WHAT IS MY EMAIL PROVIDER?
 * ==========================
 *
 * Email merupakan sebuah cara untuk kita berinteraksi antar satu dengan yang lainnya secara elektronik,
 * Banyak sekali provider yang menyediakan layanan email ini.
 *
 * Buatlah sebuah proses yang akan mengeluarkan output provider email yang digunakan oleh user.
 *
 * Contoh:
 *   - Input  : icha@hacktiv8.com
 *   - Output : Your email provider is hacktiv8
 *     (tidak menggunakan .com di belakang)
 * 
 * RULES:
 *   - Tidak diperbolehkan menggunakan built-in function:
 *     .map .filter .reduce .split .join .indexOf .findIndex .substring
 */

let mail = 'icha@hacktiv8.com'


// Your code here


let startMailProvider = false;
let mailProvider = '';
for(let i=0;i<mail.length;i++){
    
    if(startMailProvider){
        if(mail[i] == '.'){
            break;
        }
        mailProvider += mail[i];
    }
    if(mail[i] == '@'){
        startMailProvider = true;
    }
}

console.log(mailProvider);