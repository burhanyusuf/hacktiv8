function generateHouse(height,heightDoor,widthDoor){
    let output = "";
    let startDoor = height - Math.ceil(widthDoor/2);
    let endDoor = startDoor + widthDoor -1
    for(let i=0;i<height-1;i++){
        output = "";
        for(let j=0;j<(height*2 - 1);j++){
            if(j == (height - 1) - i || j == height + i-1){
                output += ("* ")
            }else{
                output += ("  ")
            }
        }
        console.log(output)
    }
    output = ""
    for(let i = 0;i<(height * 2) -1;i++){
        output += ("* ")
    }
    console.log(output)
    for(let i= 0; i < height -1;i++){
        output = "";
        for(let j=0;j < (height*2) -1;j++){
            if(j == 1 || j == (height*2) - 3){
                output += "* "
            }else if((i == height - heightDoor) && j >= startDoor && j<= endDoor){
                output += "# "
            }else if((i > height - heightDoor) && (j == startDoor || j == endDoor)){
                output += "# "
            }
            else{
                output += "  "
            }
        }
        console.log(output)
    }
    output = ""
    for(let i = 0;i<(height * 2) -2;i++){
        if(i < startDoor || i > endDoor){
            if(i != 0)
                output += ("* ")
            else
                output += ("  ")
        }else if(i >= startDoor && i<= endDoor){
            output += ("# ")
        }
        else
            output += "  "
    }
    console.log(output)
}

generateHouse(7, 4, 3)