/**
 * DISNEY ISLAND
 * =============
 * 
 * Buatlah pseudocode untuk kasus berikut:
 * 
 * Sebuah wahana bermain 'Disney Island' akan memberikan tarif sesuai dengan tinggi dan umur anak
 * dengan ketentuan sebagai berikut:
 *   - Umur 1 tahun ke bawah: Tampilkan 'Dilarang masuk'
 *   - Umur 2-3 tahun: Rp 30.000. Jika tinggi anak umur 2-3 tahun lebih dari 70cm maka tarif akan bertambah Rp. 10.000.
 *   - Umur 4-7 tahun: Rp 40.000. Jika tinggi anak umur 4-7 tahun lebih dari 120cm maka tarif akan bertambah Rp. 15.000.
 *   - Umur 8-10 tahun: Rp 50.000. Jika tinggi anak umur 8-10 tahun lebih dari 150cm maka tarif akan bertambah Rp. 20.000.
 *   - Umur diatas 10 tahun : Rp 80.000.
 * 
 * Tampilkan tarif harga sesuai umur dan tinggi seorang anak!
 */

/**
PSEUDOCODE

STORE tinggi as NUMBER WITH 100
STORE umur as NUMBER WITH ANY 3
STORE harga as NUMBER WITH ANY VALUE

IF umur LESS THAN 1
    DISPLAY "Dilarang masuk"
ELSE IF umur MORE THAN 1 AND LESS THAN 4
    SET harga WITH 30000
    IF tinggi MORE THAN 70
        SET HARGA with HARGA PLUS 10000
    END IF
    DISPLAY HARGA
ELSE IF umur MORE THAN 3 AND LESS THAN 8
    SET harga WITH 30000
    IF tinggi MORE THAN 120
        SET HARGA with HARGA PLUS 15000
    END IF
    DISPLAY HARGA
ELSE IF umur MORE THAN 7 AND LESS THAN 11
    SET harga WITH 30000
    IF tinggi MORE THAN 150
        SET HARGA with HARGA PLUS 20000
    END IF
    DISPLAY HARGA
ELSE 
    SET harga WITH 80000
    DISPLAY HARGA
END IF

 
*/
