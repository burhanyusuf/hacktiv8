let angka =33;                                          
let palindrome = '';

while (true) {
    palindrome = ""
    if (angka >= 0 && angka <= 8) {
        palindrome = angka + 1
        palindrome = palindrome.toString();
        break;
    }

    angka++;                                        //=> Jadi angka 10

    angka = angka.toString();                               //=> "10"

    for (let i = angka.length - 1 ; i >= 0; i--) {
        palindrome += angka[i];                     //=> palindrome = "10"
    }
    if (angka === palindrome) {                     //=> "10" === 10 // false
        break;
    }

}

console.log(palindrome)


