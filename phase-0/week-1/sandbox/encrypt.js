let kamus_huruf = 'abcdefghijklmnopqrstuvwxyz';

function caesar_encrypt(kata, geser){

    let result = '';
    for(let i=0;i<kata.length;i++){
        for(let j=0;j<kamus_huruf.length;j++){
            if(kata[i] == kamus_huruf[j]){
                result += kamus_huruf[(j+geser) % 26];
                break
            }
        }
        if(!(isNaN(kata[i]))){
            result+=kata[i];
        }
    }
    return result;
}

function caesar_decrypt(kata, geser){
    let result = '';
    for(let i=0;i<kata.length;i++){
        for(let j=0;j<kamus_huruf.length;j++){
            if(kata[i] == kamus_huruf[j]){
                result += kamus_huruf[((26+j)-geser) % 26];
                break
            }
        }
        if(!(isNaN(kata[i]))){
            result+=kata[i];
        }
    }
    return result;
}

console.log(caesar_encrypt('hacktiv8',2));
console.log(caesar_encrypt('xyz',4));
console.log(caesar_encrypt('aku anak sehat', 1))

console.log(caesar_decrypt('jcemvkx8', 2)) // hacktiv8
console.log(caesar_decrypt('bcd', 4)) // xyz
console.log(caesar_decrypt('blv bobl tfibu', 1)) 