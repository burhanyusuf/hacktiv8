function decimalToBinary(angka) {
    let output = "";
    do{
        bin = angka % 2;
        angka = Math.floor(angka / 2)
        output += "" + bin
    }while(angka != 0) 
    let result = "";
    for(let i=output.length-1;i>=0;i--){
        result += output[i];
    }
    return result
  }
  
  console.log(decimalToBinary(0))  // 0
  console.log(decimalToBinary(1))  // 1
  console.log(decimalToBinary(2))  // 10
  console.log(decimalToBinary(5))  // 101
  console.log(decimalToBinary(20)) // 10100
  
  function binaryToDecimal(binary) {
    // your code here
    let result = 0;
    for(let i=binary.length-1;i>=0;i--){
        result = result + (Number(binary[i]) * Math.pow(2,i))
    }
    return result;
  }
  
console.log(binaryToDecimal('0'))     // 0
console.log(binaryToDecimal('1'))     // 1
console.log(binaryToDecimal('101'))   // 5
console.log(binaryToDecimal('11011')) // 27