function splitter(str,char){
    let output = [];
    let tmp = "";
    for(let i=0;i<str.length;i++){
        if(str[i] == char){
            output.push(tmp);
            tmp = [];
        }else{
            tmp += str[i];
        }
        if(i === str.length - 1){
            output.push(tmp);
        }
    }
    return output;
}

function changeData(str){

    let result = splitter(str,';');
    let data = [];
    for(let i = 0;i<result.length;i++){
        let tmp = splitter(result[i],':');
        data.push(tmp[1]);
    }
    return data;

}

let input  = 'nama:Afifah;umur:23;alamat:Bogor';
console.log(changeData(input));