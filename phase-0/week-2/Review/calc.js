function calculator(angka,operator){
    let result = angka[0];
    switch(operator){
        case '+':
            for(let i=1;i<angka.length;i++){
                result = result + angka[i];
            }
            break;
        case '-':
            for(let i=1;i<angka.length;i++){
                result -= angka[i];
            }
            break;
        case '*':
            for(let i=1;i<angka.length;i++){
                result *= angka[i];
            }
            break;
        case '/':
            for(let i=1;i<angka.length;i++){
                result /= angka[i];
            }
            break;
        case '%':
            for(let i=1;i<angka.length;i++){
                result %= angka[i];
            }
            break;
        default :
            result = 'Operator salah';
    }
    return result;
}

console.log('+:', calculator([1,2,3],'+'));
console.log('-:', calculator([1,2,3],'-'));
console.log('*:', calculator([1,2,3],'*'));
console.log('/:', calculator([1,2,3],'/'));
console.log('%:', calculator([1,2,3],'%'));

