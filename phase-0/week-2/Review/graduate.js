function graduatesCount(scores, minimumGrade = -1) {
    // tulis code di sini...
    let result =[];
    if(scores.length == 0){
        return result;
    }else if(minimumGrade == -1){
        return 'Masukan minimum grade';
    }
    for(let i=0;i<scores.length;i++){
        if(scores[i] >= minimumGrade){
            result.push(scores[i]);
        }
    }
    if(result.length == scores.length){
        return 'Semua orang lulus';
    }else if(result.length == 0){
        return 'Tidak ada yang lulus';
    }else{
        return `${result.length} orang lulus`;
    }
}

console.log(graduatesCount([80, 79, 60, 72, 73], 72))
console.log(graduatesCount([80, 79, 60, 72, 73], 78))
console.log(graduatesCount([40, 70, 60, 72], 75))
console.log(graduatesCount([80, 79, 72, 73], 60))
console.log(graduatesCount([79, 60, 72, 73]))
console.log(graduatesCount([], 72))