function getBoardNumbers(num){
    let result = [];
    let counter = 1;
    for(let i=1;i<=num;i++){
        let tmp_result = [];
        for(let j=1;j<=num;j++){
            tmp_result.push(counter);
            counter++;
        }
        result.push(tmp_result)
    }
    return result;
}

console.log(getBoardNumbers(4));