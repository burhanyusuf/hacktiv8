const hilangDuplikat = (arr) => {

    let result = "";
    let duplikat = "";
    let tanpaDuplikat = "";
    for(let i=0;i<arr.length;i++){
        let find = false;
        for(let j=0;j<result.length;j++){
            if(arr[i] == result[j]){
                find = true;
                duplikat += (arr[i]);
                break;
            }
        }
        if(!find){
            result += arr[i];
        }
    }
    for(let i=0;i<result.length;i++){
        let find = false;

        for(let j=0;j<duplikat.length;j++){
            if(result[i] == duplikat[j]){
                find = true;
                break;
            }
        }
        if(!find){
            tanpaDuplikat += result[i];
        }
    }
    return tanpaDuplikat;
}

// console.log(hilangDuplikat('aabbbccdd'));
console.log(hilangDuplikat('raihan'));
console.log(hilangDuplikat('hello'));