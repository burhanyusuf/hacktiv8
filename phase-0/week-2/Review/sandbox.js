
let arr = [
  [0, 0, 1, 0],
  [1, 1, 0, 0],
  [0, 1, 0, 1],
  [1, 0, 1, 1]
]


// output:
// [
//     [1, 1, 0, 1],
//     [0, 0, 1, 1],
//     [1, 0, 1, 0],
//     [0, 1, 0, 0]
// ]


// Cara
// 1. buat functio flipzero
// 2. bikin variable result
// 3. di looping arr luarnya/ arr.length
// 4. buat variable perBaris buat nampung array dalamnya
// 5. buat variable resultPerbaris buat nampung hasil arraynya
// 6. buat looping ke dua, dari array perBaris / perBaris.length
// 7. buta variable perCeil untuk nampung pecahan data dari array perBaris
// 8a.  kondisi jika ketemu number 1 makan di perBaris[j] diganti jadi 0
// 8b.  kondisi jika ketemu number 0 makan di perBaris[j] diganti jadi 1
// 9. di push hasil dari kondisi ke result
// 10. return


function flipZero(array) {
    let result =[];
    for(let i=0;i<array.length;i++){
        let perBaris = array[i];
        let resultPerbaris = [];
        for(let j=0;j<perBaris.length;j++){
            if(perBaris[j] == 1){
                resultPerbaris.push(0)
            }else{
                resultPerbaris.push(1)
            }
        }
        result.push(resultPerbaris);
    }
    return result;
}

console.log(flipZero(arr));


/**
 * =================
 * Modular Function Calculator
 * =================
 *
 * Buatlah sebuah fungsi kalkulator yang menerima dua paramter
 * berupa angka array dan satu parameter berupa operator
 *
 * dan nanti saat penjumlahan di jumlahkan di function yg berbeda atau disendirikan. misal ada function pejumlahan, perkalian dll
 *
 * Operator yang akan kita lakukan adalah
 * + -> penjumlahan angka tersebut
 * - -> pengurangan angka tersebut
 * * -> perkalian angka tersebut
 * / -> pembagian angka tersebut
 * % -> moduls angka tersebut
 */

// Buatlah modular function dan code di sini...
function penjumlahan(a,b){
    return a + b;    
}

function pengurangan(a,b){
    return a - b;    
}

function perkalian(a,b){
    return a * b;    
}

function pembagian(a,b){
    return a / b;
}

function modulus(a,b){
    return a % b;
}

// Buat fungsi dan code di sini...
function calculator(angka, operator) {
    let result = angka[0];
    for(let i=1;i<angka.length;i++){
        switch(operator){
            case '+':
                result = penjumlahan(result,angka[i]);
                break;
            case '-':
                result = pengurangan(result,angka[i]);
                break;
            case '*':
                result = perkalian(result,angka[i]);
                break;
            case '/':
                result = pembagian(result,angka[i]);
                break;
            case '%':
                result = modulus(result,angka[i]);
                break;
        }
    }
    return result;
}


console.log(calculator([10,3,1,9,0,0],'+'))
console.log(calculator([9,8,3,10,11,0],'-'))



/*
* Hilang Duplikat
 * =====================
 *
 * Terdapat function hilang duplikat yang berfungsi untuk menghilangkan nilai duplikat dalam sebuah array
 * Contoh:
 * console.log(hilangDuplikat([4, 'ayam', 'kucing', 'ayam', 1, 4]))
 * output:
 * [4, 'ayam', 'kucing', 1]
 *
 * hanya boleh pake built function in push()
 *
*/
const hilangDuplikat = (arr) => {

    let result = [];

    for(let i=0;i<arr.length;i++){
        let find = false;
        for(let j=0;j<result.length;j++){
            if(arr[i] == result[j]){
                find = true;
                break;
            }
        }
        if(!find){
            result.push(arr[i]);
        }
    }
    return result;

}




console.log(hilangDuplikat([4, 'ayam', 'kucing', 'ayam', 1, 4]))
// // output :
// // [4, 'ayam', 'kucing', 1]

console.log(hilangDuplikat([4, 'ayam', 'kucing', 'ayam', 1, 4, 'kucing', 'kucing']))
// // // [4, 'ayam', 'kucing', 1]


console.log(hilangDuplikat([1, 1, 1, 2]))
// // // [1, 2]







// Telegram
// cara penulisan di telegram berbeda dengan cara tulis surat.
// semua titik dan koma ditulis "titik" dan "koma" bukan "." dan ","
// selesaikan aplikasi di bawah untuk membuat pesan telegram dari string biasa.

function convertTitik(str){
    let result = "";
    for(let i=0;i<str.length;i++){
        if(str[i] == '.'){
            result += "titik";
        }else{
            result += str[i];
        }
    }
    return result;
}

function convertKoma(str){
    let result = "";
    for(let i=0;i<str.length;i++){
        if(str[i] == ','){
            result += "koma";
        }else{
            result += str[i];
        }
    }
    return result;
}

function convertTelegram(str){
    let result = '';
    result = convertTitik(str);

    result = convertKoma(result);
    return result;

}

console.log(convertTelegram("no, price too high."))
// // nokoma price to highttitik
console.log(convertTelegram("hm... boleh juga? tapi, kemahalan ngk?"))
// // hmtitiktitiktitik boleh juga? tapikoma kemahalan ngk?