function isEqual(a,b){
    let hasil = []
    let output = true;
    for (let property in a) {
        hasil.push(property.toLowerCase());
    }
      // console.log(hasil)
    hasil2 = []
    for (let key in b) {
        hasil2.push(key.toLowerCase());
    }
    if (hasil.length !== hasil2.length) {
        return false
    }

    for(let i=0;i<hasil.length;i++){
        let tmpOutput = false;
        for(let j=0;j<hasil2.length;j++){
            if(hasil2[j] == hasil[i]){
                tmpOutput = true;
            }
        }
        if(!tmpOutput){
            output = false;
            break;
        }
    }

    return output;
}

let a
let b

a = {
  foo: 1,
  bar: 2
}

b = {
  Foo: 8,
  bAR: 100
}

console.log(isEqual(a, b)) // true

a = {
  foo: 1,
  bar: 2,
  yeah: 9
}

b = {
  Foo: 8,
  bAR: 100
}

console.log(isEqual(a, b)) // false

a = {
  foo: 1,
  bar: 2
}

b = {
  Foo: 8,
  bARe: 100
}

console.log(isEqual(a, b)) // false