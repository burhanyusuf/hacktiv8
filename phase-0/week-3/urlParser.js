function urlParser(url){
    url = url.toLowerCase()
    let flag = {
        protocol : true,
        domain : false,
        username : false
    }
    let output = {
        protocol : "",
        domain : "",
        username : ""
    }

    for(let i=0;i<url.length;i++){

        if(flag.protocol){
            if(url[i] >= 'a' && url[i] <= 'z'){
                output.protocol += url[i];
            }

            if(url[i] == '/' && url[i-1] == '/'){
                flag.protocol = false;
                flag.domain = true;
            }
        }else if(flag.domain){
            
            if(url[i] == '/' || url[i+1] == undefined){
                flag.domain = false;
                flag.username = true;
                continue
            }
            output.domain += url[i];
        }else if(flag.username){
            output.username += url[i]
        }
    }

    if(output.username == ""){
        output.username = 'not available'
    }

    return output;
}


console.log(urlParser('https://facebook.com/foo'))

console.log(urlParser('http://imgur.co.id/'))
