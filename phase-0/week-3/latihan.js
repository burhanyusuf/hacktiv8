/**
 * Objective: Memahami pengecekan data di dalam object
 *
 * Misalkan kita memiliki sebuah object seperti ini
 * {
 *  name: 'Kosasih',
 *  grade: 90
 * }
 *
 * Buatlah sebuah fungsi yang akan melakukan pengecekan apakah sebuah property ada di dalam object
 */

function hasProperty(obj, key) {
    return obj[key] !== undefined
}

console.log(
  hasProperty(
    {
      name: 'Kosasih',
      grade: 90
    },
    'grade'
  )
) // true
console.log(
  hasProperty(
    {
      name: 'Kosasih',
      grade: 90
    },
    'gender'
  )
) // false


// ========================================================================================================
// ========================================================================================================
// ========================================================================================================


/**
 * Objective: Grouping collection of data into an object
 *
 * Kosasih memiliki sebuah peternakan dengan kumpulan hewan sebagai berikut
 * [ 'Ayam', 'Bebek', 'Ayam', 'Kuda', 'Kuda']
 *
 * Buatlah sebuah fungsi yang akan menerima input sebuah array seperti diatas, dan akan mengembalikan
 * Sebuah object jumlah hewan yang dimiliki oleh Kosasih dikelompokan berdasarkan namanya.
 * Liat driver code untuk output yang lebih jelas
 */

function groupAnimal(array) {

    let object = {};
    for(let i=0;i<array.length;i++){
      if(object[array[i]] == undefined){
        object[array[i]] = 1;
      }else{
        object[array[i]]++;
      }
    }
    return (object)
}

console.log(groupAnimal(['Ayam', 'Bebek', 'Ayam', 'Kuda', 'Kuda']))
/**
 * {
 *  Ayam: 2,
 *  Kuda: 2,
 *  Bebek: 1
 * }
 */


// ========================================================================================================
// ========================================================================================================
// ========================================================================================================



/**
 * =============
 * Company Match
 * =============
 *
 *
 * Company match adalah sebuah fungsi yang akan mencocokan requirement dari sebuah perusahaan dengan skill yang dimiliki oleh seseorang.
 *
 * fungsi ini akan menerima dua parameter yang berupa sebuah object.
 *
 * Parameter pertama adalah sebuah object perusahaan dengan beberapa data seperti: nama perusahaan, job yang ditawarkan, dan requirement yang dibutuhkan untuk memdapatkan pekerjaan
 * Parameter kedua adalah sebuah object pelamar dengan beberapa data seperti: nama pelamar, skill pelamar, dan job yang dinginkan oleh pelamar tersebut.
 *
 * Fungsi ini akan menghitung persentase berapa cocok perusahaan dan pelamar yang dikirimkan dengan beberapa syarat.
 * 1. Jika job yang ditawarkan oleh perusahaan tidak sesuai dengan job yang diinginkan maka persentase mereka adalah 0%
 * 2. Jika seluruh requirement yang diminta oleh perusahaan dipenuhi oleh pelamar maka persentase mereka adalah 100%.
 * 3. Jika ada beberapa requirement yang tidak dipenuhi pelamar maka persentase akan dihitung dengan rumus [ jumlah requirement yang dipenuhi] / [ requirement yang diminta]
 *    contoh: Terdapat 3 requirement pada perusahan tetapi pelamar hanya memenuhi 2 requirement oleh karena itu persentase pelamar tersebut adalah 66% ( dibulatkan ke bawah)
 *
 * Jika seseorang mendapatkan persentase diatas 60% maka tampilkanlah pesan `Selamat [nama pelamar] anda cocok dengan perusahaan [ nama perusahaan] dengan persentase kecocokan [ persentas]
 * Jika tidak maka tampilkan pesan `Mohon maaf [nama pelamar] anda belum cocok dengan perusaahan [nama perusaahn] anda hanya mendapatkan persentase kecocokan [ persentase]
 */

function companyMatch(company, user) {
  // Insert your code here
  let persen = 0;
  let cocok = 0;
  for(let i=0;i<company.requirement.length;i++){
      for(let j=0;j<user.skills.length;j++){
        if(company.requirement[i] == user.skills[j]){
            cocok++;
            break;
        }
      }
  }
  persen = Math.floor((cocok / company.requirement.length) * 100);
  if(persen > 60){
      return `Selamat ${user.name} anda cocok dengan perusahaan ${company.name} dengan persentase kecocokan ${persen}%`
  }else{
      return `Mohon maaf ${user.name} anda belum cocok dengan perusaahan ${company.name} anda hanya mendapatkan persentase kecocokan ${persen}%`
  }
}

const company1 = {
  name: 'Pesbok',
  job: 'Frontend Developer',
  requirement: ['HTML', 'CSS', 'JS']
}

const john = {
  name: 'John',
  applyAs: 'Frontend Developer',
  skills: ['HTML', 'CSS', 'JS']
}

const kosasih = {
  name: 'Kosasih',
  applyAs: 'Backend Developer',
  skills: ['Express', 'Node', 'PHP']
}

const marry = {
  name: 'Marry',
  applyAs: 'Frontend Developer',
  skills: ['HTML', 'CSS']
}

console.log(companyMatch(company1, john)) // Selamat John anda cocok dengan perusahaan Pesbok dengan persentase kecocokan 100%
console.log(companyMatch(company1, kosasih)) // Mohon maaf Kosasih anda belum cocok dengan perusaahan Pesbok anda hanya mendapatkan persentase kecocokan 0%
console.log(companyMatch(company1, marry)) // Selamat Marry anda cocok dengan perusahaan Pesbok dengan persentase kecocokan 66%


// ========================================================================================================
// ========================================================================================================
// ========================================================================================================


/**
 * =================
 * SCREENING BANDARA
 * =================
 * 
 * Kalian ditugaskan untuk membuat sebuah program yang akan dipakai Bandara Internasional Soekarno-Hatta untuk memeriksa apakah seseorang merupakan suspect 
 * dari suatu penyakit berbahaya yang baru saja mewabah akhir-akhir ini.
 * Program akan menerima input berupa object yang merepresentasikan penumpang dengan format seperti berikut:
{
    name: (string),
    id: (Number),
    temperature: (Number),
    travelHistory: [] (Array of Strings),
}
 * Program ini akan mengevaluasi apakah orang ini merupakan suspect dari penyakit ini atau tidak, dan mengeluarkan output berupa string. 
 * Berikut ini adalah kondisi-kondisi yang akan di handle program ini:
 * - Jika suhu tubuhnya diatas 35 derajat dan dia pernah mengunjungi salah satu dari 4 negara berikut: 'China', 'Japan', 'Korea', dan 'Singapore' pada travel historynya, maka dia dinyatakan sebagai "Suspect".
 * - Jika suhu tubunya tidak diatas 35 derajat, maka dia tidak apa-apa dan dinyatakan "Healthy".
 *      - Namun, jika suhunya tidak diatas 35 derajat tetapi dia pernah berkunjung ke 4 negara diatas, maka dia dinyatakan sebagai seorang "Potential Carrier".
 * - Jika suhu tubuhnya tinggi tetapi dia tidak pernah berkunjung ke 4 negara tersebut maka dia hanya sakit, dan dinyatakan "Sick".
 * 
 * Output yang dikeluarkan program ini memiliki format sebagai berikut:
 * "Passenger [id] [name] [status]"
 * 
 * dimana status bisa berupa:
 * [status] : Suspect, Healthy, Potential Carrier, Sick 
 * 
 * 
 * RULES
 * - Dilarang menggunakan built-in function kecuali .push()
 *
 */

function evaluatePassenger(passenger) {
  let warnCountry = ['Chine','Japan','Korea','Singapore'];
  let suhuMax = 35;
  let isSuhuMelewatiBatas = false;
  let isMengunjungi = false;
  let status = 'healthy';

  for(let i=0;i<passenger.travelHistory.length;i++){
      for(let j=0;j<warnCountry.length;j++){
          if(passenger.travelHistory[i] == warnCountry[j]){
              isMengunjungi = true;
          }
      }
  }

  if(passenger.temperature > suhuMax){
    isSuhuMelewatiBatas = true;
  }

  if(isSuhuMelewatiBatas && isMengunjungi){
      status = 'Suspect';
  }else if(isSuhuMelewatiBatas && !isMengunjungi){
      status = 'Sick';
  }else if(!isSuhuMelewatiBatas && isMengunjungi){
      status = 'Potential Carrier';
  }

  return `Passenger ${passenger.temperature} ${passenger.name} ${status}`;
  
}

console.log(evaluatePassenger({name: 'Budi', id: 50, temperature: 40, travelHistory: ['Russia', 'Japan']})) //Passenger 50 Budi Suspect
console.log(evaluatePassenger({name: 'Tono', id: 10, temperature: 40, travelHistory: ['Morocco', 'France', 'Burma']})) //Passenger 10 Tono Sick
console.log(evaluatePassenger({name: 'Tsubasa', id: 15, temperature: 30, travelHistory: ['Brazil']})) //Passenger 15 Tsubasa Healthy


// ========================================================================================================
// ========================================================================================================
// ========================================================================================================


/*
============
Battle Stats
============
Diberikan array 2 dimensi yang merupakan rangkuman hasil dari pertempuran 2 kelompok pasukan. Di tiap pasukan, terdapat pasukan-pasukan kecil yang dipimpin oleh Jendralnya masing-masing.

Jika angka Jendral positif, maka di sebuah pertempuran, pasukan Jendral tersebut menang sebanyak angka tersebut
Jika angka Jendral negatif, maka di sebuah pertempuran, pasukan Jendral tersebut kalah sebanyak angka tersebut

Tugas kamu untuk mengeluarkan statistik hasil pertempuran seperti contoh

Contoh :
let battle = [
  ["Hwang Shi Mok", -1],
  ["Savants", 1],
  ["Hwang Shi Mok", 2],
  ["Hwang Shi Mok", 1],
  ["Savants", 5],
  ["Legolas", 10],
  ["Legolas", -20],
  ["Legolas", -10],
]

console.log(battleStats(battle))

Output :
{
  "Hwang Shi Mok": {
    "Kalah": 1,
    "Menang": 3
  },
  "Savants": {
    "Kalah": 0,
    "Menang": 6
  },
  "Legolas": {
    "Kalah": 30,
    "Menang": 10
  },
}

*/

function battleStats(battle){
    let output = {};
    for(let i=0;i<battle.length;i++){
        let currentJendral = battle[i];
        if(output[currentJendral[0]] == undefined){
            output[currentJendral[0]] = {
                "Kalah" : 0,
                "Menang" : 0
            }
        }
        if([currentJendral[1]] > 0){
            output[currentJendral[0]]["Menang"]+= currentJendral[1];
        }else if([currentJendral[1]] < 0){
            output[currentJendral[0]]["Kalah"] += (currentJendral[1] * -1);
        }
    }
    return output;
}
console.log(battleStats(
    [
      ["Hwang Shi Mok", -1],
      ["Savants", 1],
      ["Hwang Shi Mok", 2],
      ["Hwang Shi Mok", 1],
      ["Savants", 5],
      ["Legolas", 10],
      ["Legolas", -20],
      ["Legolas", -10],
    ]
  )
)


// ========================================================================================================
// ========================================================================================================
// ========================================================================================================



/*
BELANJA KE WARUNG
====================
Pada suatu hari, Reno di perintahkan oleh ibunya untuk belanja ke warung. 
Sesampainya di warung terdapat sebuah list dari barang yang tersedia di warung, 
Reno hanya akan membeli barang jika barang tersebut tersedia di warung.
Buatlah sebuah function yang akan menghitung total belanjaan dari reno, 
funtion menerima 1 parameter berupa array multidimensi dari barang yang di perintahkan oleh ibu untuk di beli dan jumlah yang harus dibeli. 
Function akan mengembalikan total harga belanjaan yang harus dibayar oleh reno
contoh : 
input : [
  ["Beras", 3], 
  ["Telur", 10], 
  ["Gula", 0.5], 
  ["Minyak", 5], 
  ["Tepung", 4]
]
barang tersedia :
{
  Melon : 15000,
  Beras : 8000,
  Telur : 1500,
  Gula : 12000,
  Minyak : 8000,
  Permen : 500,
  Sabun : 10000,
  Shampo : 9500,
  Pewangi : 17000
}
output : "Total Belanjaan yang harus dibayar adalah : Rp.85000"
*/

function belanjaWarung(arr){
    if(arr.length == 0){
        return 'Tidak perlu pergi ke warung'
    }
  let barangTersedia = {
    Melon : 15000,
    Beras : 8000,
    Telur : 1500,
    Gula : 12000,
    Minyak : 8000,
    Permen : 500,
    Sabun : 10000,
    Shampo : 9500,
    Pewangi : 17000
  }
  let total = 0;
  for(let i=0;i<arr.length;i++){
    let currBelanja = arr[i];
    total+= !barangTersedia[currBelanja[0]] ? 0 : (barangTersedia[currBelanja[0]] * currBelanja[1]);
  }
  return `Total Belanjaan yang harus dibayar adalah : Rp.${total}`
}

const belanja1 = [
  ["Beras", 3], 
  ["Telur", 10], 
  ["Gula", 0.5], 
  ["Minyak", 5], 
  ["Tepung", 4]
]

console.log(belanjaWarung(belanja1))//Total Belanjaan yang harus dibayar adalah : Rp.85000

const belanja2 = [
  ["Mobil",1],
  ["PS5", 4],
  ["Melon",5],
  ["Permen",100]
]
console.log(belanjaWarung(belanja2))//Total Belanjaan yang harus dibayar adalah : Rp.125000
console.log(belanjaWarung([]))//Tidak perlu pergi ke warung


// ========================================================================================================
// ========================================================================================================
// ========================================================================================================


/*
PEMILU HOKAGE KE-12
=====================
Dibeberapa desa sedang diadakan pemilu hokage ke 12, 
orang terpilih di tiap desa akan di angkat sebagai hokage di tiap desanya. 
tiap warga akan mengusulkan 1 nama yang akan di pilih, 
nama yang paling banyak di usulkan akan di angkat sebagai hokage, 
warga juga berhak tidak mengusulkan nama. 

Buatlah sebuah function yang akan membantu perhitungan pemilu kali ini.
Function akan menerima 1 parameter berupa array multidimensi dengan format [ [<nama warga>, <nama yang diusulkan>] ]
Funciton akan mengembalikan object dengan format :
{
  <nama yang diusulkan> : [<nama warga>]
  <nama yang diusulkan> : [<nama warga>]
  golput : [<nama warga>] // Hanya jika ada yang tidak mengusulkan nama
  hokage terpilih : <warga yang paling banyak diusulkan>
}

contoh : 
input : [
  ["Sikura", "Narto"],
  ["Saske", "Borto"],
  ["Ichita", ""],
  ["Takkashi", "Narto"],
  ["Hinaya", "Narto"],
  ["Orochimiru", ""]
]
outut : {
  Narto : ["Sikura", "Takkashi", "Hinaya"],
  Borto : ["Saske"],
  Golput : ["Ichita", "Orochimiru"] 
  "Hokage Terpilih" : "Narto"
}
*/

function pemilu(arr){
  let output = {
  };
  let Golput = [];
  let highest = {nama : "",jumlah: 0};
  for(let i=0;i<arr.length;i++){
      let currentData = arr[i];
      if( currentData[1] && output[currentData[1]] == undefined){
          output[currentData[1]] = [];
      }
      if(currentData[1]){
        output[currentData[1]].push(currentData[0]);
      }else{
          Golput.push(currentData[0]);
      }
  }
  for(let key in output){
    if(output[key].length > highest.jumlah){
        highest.nama = key
        highest.jumlah = output[key].length
    }
  }
  if(Golput.length > 0){
      output.Golput = Golput;
  }
  output["Hokage Terpilih"] = highest.nama;
  return output;
}

const desa1 = [
  ["Sikura", "Narto"],
  ["Saske", "Borto"],
  ["Ichita", ""],
  ["Takkashi", "Narto"],
  ["Hinaya", "Narto"],
  ["Orochimiru", ""]
]
console.log(pemilu(desa1))
/*
{
  Narto : ["Sikura", "Takkashi", "Hinaya"],
  Borto : ["Saske"],
  Golput : ["Ichita", "Orochimiru"]
  "Hokage Terpilih" : "Narto"
}
*/

const desa2 = [
  ["Jariya", "Tsudena"],
  ["Neja", "Raga"],
  ["Katubo", "Raga"],
  ["Jicho", "Tsudena"],
  ["Saori", "Raga"]
]
console.log(pemilu(desa2))
/*
{
  Tsudena : ["Jariya", "Jicho"],
  Raga : ["Neja", "Katubo", "Saori"],
  "Hokage Terpilih" : "Raga"
}
*/

// ========================================================================================================
// ========================================================================================================
// ========================================================================================================



/**
 * TRAVEL PLANNER
 * ==================
 * 
 * Travel Planner adalah sebuah function yang menerima 1 parameter berupa 
 * array of object dari list peserta travel, function ini akan mengembalikan sebuah
 * object yang berisikan list nama peserta travel,total budget dan tujuan peserta
 * 
 * function travelPlanner ini merupakan sebuah modular function yang nantinya akan
 * melakukan pemanggilan function lain :
 *  - function filterGroup adalah sebuah function yang akan melakukan filter pada 
 *    group, filter dilakukan untuk menghindari adanya anggota yang umurnya masih
 *    dibawah 17 tahun dan budget kurang dari seratus ribu
 * - function sumBudget adalah sebuah function yang akan menghitung jumlah budget
 *   keseluruhan peserta yang telah lolos filter
 * 
 * Setelah di dapatkan total budget peserta, function travel planner akan menentukan
 * tujuan destinasi liburan untuk anggota yang telah lulus filter dengan ketentuan
 * sebagai berikut
 *    - jika budget lebih dari sama dengan satu juta, tujuan adalah Bandung
 *    - jika budget lebih dari tiga juta, tujuan adalah Jogjakarta
 *    - Jika budget lebih dari tujuh juta, tujuan adalah Bali
 *    - Jika budget kurang dari satu juta, tujuan adalah Ancol
 * 
 * Contoh :
 * input : [
    {name : "Stuart", age : 30, budget: 200000},
    {name : "Angela", age : 15, budget: 30000},
    {name : "Brenda", age :20, budget:180000},
    {name : "Jhon", age :40, budget:1800000},
    {name : "Albert", age :16, budget: 120000}
   ]
   output : {
     peserta : ["Stuart", "Brenda", "Jhon"],
     totalBudget : 2180000,
     tujuan : "Bandung"
   }
 * 
 */

function sumBudget(arr){
    let output = {
        peserta : [],
        totalBudget : 0,
        tujuan : ""
    }

    for(let i=0;i<arr.length;i++){
        let currObj = arr[i];
        output.peserta.push(currObj.name)
        output.totalBudget += currObj.budget
    }

    if(output.totalBudget > 7000000){
        output.tujuan = "Bali";
    }else if(output.totalBudget > 3000000){
        output.tujuan = "Jogjakarta";
    }else if(output.totalBudget >= 1000000){
        output.totalBudget = "Bandung"
    }else{
        output.totalBudget = "Ancol"
    }

    return output;
}

function filterGroup(arr){
    let result = [];
    for(let i = 0;i<arr.length;i++){
        let currObj = arr[i];
        if(currObj.age >= 17 && currObj.budget >= 100000){
            result.push(currObj);
        }
    }
    return result;
}

function travelPlanner(arr){
    if(typeof arr != "object" || arr.length == 0){
        return "Invalid Input";
    }

    let output = filterGroup(arr);
    output = sumBudget(output);

    return output;
}

let group1 = [
  {name : "Stuart", age : 30, budget: 200000},
  {name : "Angela", age : 15, budget: 30000},
  {name : "Brenda", age :20, budget:180000},
  {name : "Jhon", age :40, budget:1800000},
  {name : "Albert", age :16, budget: 120000}
]
console.log(travelPlanner(group1))
/**
  {
    peserta : ["Stuart", "Brenda", "Jhon"],
    totalBudget : 2180000,
    tujuan : "Bandung"
  }
 */

let group2 = [
  { name: "Steven", age: 18, budget: 1000000 },
  { name: "Jesica", age: 20, budget: 2000000 },
  { name: "Kelvin", age: 25, budget: 3500000 },
  { name: "Susan", age: 23, budget: 800000 },
  { name: "Mikela", age: 19, budget: 950000 }
]
console.log(travelPlanner(group2))
/**
  {
    peserta : ["Steven", "Jesica", "Kelvin", "Susan", "Mikela"],
    totalBudget : 8250000,
    tujuan : "Bali"
  }
 */

 console.log(travelPlanner([])) // Invalid Input
 console.log(travelPlanner(800000)) //Invalid Input


